<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/js/slick/slick.css" rel="stylesheet">
<link href="/assets/js/slick/slick-theme.css" rel="stylesheet">
<link href="/assets/css/style.css" rel="stylesheet">
<script src="/assets/js/jquery-2.2.4.min.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

	<header id="header" class="header">

		<?php
		//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
		// top
		//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡

		if($id=="index"){ ?>

			<div class="c-header1">
				<div class="c-header2">
					<div class="logo">
						<p>一級建築士事務所 <span>シープ</span></p>
						<h1><a href="#">SHE<span>a</span>P</a></h1>
					</div>
					<p class="text">日々をたのしむ住まい。</p>
					<ul class="nav">
						<li>Q<br>&<br>A</li>
						<li>ブログ</li>
						<li>住まいづくりのすすめ方</li>
						<li>ギャラリー</li>
						<li>デザインコンセプト</li>
						<li>シープについて</li>
					</ul>
					<div class="info">
						<img src="/assets/image/common/icon-02.png" alt="">
						<p>サイトマップ</p>
						<p>プライバシーポリシー</p>
					</div>
					<a href="#" class="home">
						<p>HOME</p>
						<p><b>へ戻る</b></p>
					</a>
				</div>
				<a href="#" class="btn-contact">
					<p>Contact</p>
					<img src="/assets/image/common/icon-01.png" alt="">
					<p><span>お問い合わせ</span></p>
				</a>
			</div>

			<?php
		//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
		// flow
		//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
		}elseif($id=="flow"){?>
			<div class="c-header1">
				<div class="c-header2">
					<div class="logo">
						<p>一級建築士事務所 <span>シープ</span></p>
						<h1><a href="#">SHE<span>a</span>P</a></h1>
					</div>
					<p class="text">日々をたのしむ住まい。</p>
					<ul class="nav">
						<li><a href="#">Q<br>&<br>A</a></li>
						<li><a href="#">ブログ</a></li>
						<li class="active"><a href="#">住まいづくりのすすめ方</a></li>
						<li><a href="#">ギャラリー</a></li>
						<li><a href="#">デザインコンセプト</a></li>
						<li><a href="#">シープについて</a></li>
					</ul>
					<div class="info">
						<img src="/assets/image/common/icon-02.png" alt="">
						<p>サイトマップ</p>
						<p>プライバシーポリシー</p>
					</div>
					<a href="#" class="home">
						<p>HOME</p>
						<p><b>へ戻る</b></p>
					</a>
				</div>
				<a href="#" class="btn-contact">
					<p>Contact</p>
					<img src="/assets/image/common/icon-01.png" alt="">
					<p><span>お問い合わせ</span></p>
				</a>
			</div>
			<?php
		//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
		// Q&A
		//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
		}
		?>

		<?php
		//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
		// Q&A
		//≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡

		if($id=="Q&A"){ ?>

			<div class="c-header1">
				<div class="c-header2">
					<div class="logo">
						<p>一級建築士事務所 <span>シープ</span></p>
						<h1><a href="#">SHE<span>a</span>P</a></h1>
					</div>
					<p class="text">日々をたのしむ住まい。</p>
					<ul class="nav">
						<li class="active"><a href="#">Q<br>&<br>A</a></li>
						<li><a href="#">ブログ</a></li>
						<li><a href="#">住まいづくりのすすめ方</a></li>
						<li><a href="#">ギャラリー</a></li>
						<li><a href="#">デザインコンセプト</a></li>
						<li><a href="#">シープについて</a></li>
					</ul>
					<div class="info">
						<img src="/assets/image/common/icon-02.png" alt="">
						<p>サイトマップ</p>
						<p>プライバシーポリシー</p>
					</div>
					<a href="#" class="home">
						<p>HOME</p>
						<p><b>へ戻る</b></p>
					</a>
				</div>
				<a href="#" class="btn-contact">
					<p>Contact</p>
					<img src="/assets/image/common/icon-01.png" alt="">
					<p><span>お問い合わせ</span></p>
				</a>
			</div>

			<?php
		}
		?>
	</header><!-- /header -->