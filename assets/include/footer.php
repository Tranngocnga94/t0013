<footer class="footer">
	<div class="c-footer1">
		<div class="l-wapper">
			<ul class="c-footer1__title">
				<li><a href="#"><img src="/assets/image/common/icon-03.png" alt=""><span>ホーム</span></a></li>
				<li><button id="myBtn" title="Go to top">ページトップへ</button></li>
			</ul>
			<ul class="c-footer1__content">
				<li>
					<div class="c-footer1__colume">
						<p class="title1"><a href="#"><span class="title1__text1">About</span><span class="title1__text2">シープについて</span></a></p>
						<p class="text">設計事務所と創りましょう<br>事務所概要<br>プロフィール<br>アクセスマップ</p>
						<p class="title1"><a href="#"><span class="title1__text1">Gallery</span><span class="title1__text2">ギャラリー</span></a></p>
						<p class="title1"><a href="#"><span class="title1__text1">Flow</span><span class="title1__text2">住まいづくりのすすめ方</span></a></p>
					</div>
				</li>
				<li>
					<div class="c-footer1__colume">
						<p class="title1"><a href="#"><span class="title1__text1">Desing</span><span class="title1__text2">デザインコンセプト</span></a></p>
						<p class="text">光<br>風<br>省エネ<br>安心・安全<br>広がり・つながり<br>シンプル</p>
						<p class="title1"><a href="#"><span class="title1__text1">Contact</span><span class="title1__text2">お問い合わせ</span></a></p>
					</div>
				</li>
				<li>
					<div class="c-footer1__colume">
						<p class="title1"><a href="#"><span class="title1__text1">Q&A</span><span class="title1__text2">質問と解答</span></a></p>
						<p class="title1"><a href="#"><span class="title1__text1">Blog</span><span class="title1__text2">ブログ</span></a></p>
						<p class="text">プライバシーポリシー<br>サイトマップ</p>
					</div>
				</li>
				<li>
					<ul class="network">
						<li><a href="#"><img src="/assets/image/common/icon-04.png" alt=""><span>公式Facebookはこちら</span></a></li>
						<li><a href="#"><img src="/assets/image/common/icon-05.png" alt=""><span>ひつじ的住まいズム！</span></a></li>
						<li><a href="#"><img src="/assets/image/common/icon-05.png" alt=""><span>ヒツジ的フォトライフ！</span></a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<div class="l-wapper">
		<div class="c-footer2">
			<div class="c-footer2__logo">
				<p>一級建築士事務所 <span>シープ</span></p>
				<a href="#">SHE<span>a</span>P</a>
			</div>
			<div class="c-footer2__info">
				<div class="c-footer2__text">
					<p>一級建築士事務所登録　静岡県知事登録（◯-00）第00000号</p>
					<p>〒400-0000静岡県浜松市中区三組町28-76</p>
					<p>TEL：053-000-0000　FAX：053-000-0000</p>
				</div>
				<div class="c-footer2__img">
					<img src="/assets/image/common/icon-06.png" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="c-footer3 js-effect1">
		<div class="l-wapper">
			<small>Copyright &copy; 2015 SHEaP inc. All Rights Reserved.</small>
		</div>
	</div>
</footer>

<script src="/assets/js/jquery.inview.min.js"></script>
<script src="/assets/js/slick/slick.js"></script>
<script src="/assets/js/functions.js"></script>
</body>
</html>