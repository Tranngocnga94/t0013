<?php /*========================================
title
================================================*/ ?>
<div class="c-dev-title1">title</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title1 (font-size: 43px)</div>
<div class="c-title1">
	<h2>スタイリッシュで<br>人と地球に優しい住まいづくり</h2>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title2 (font-size: 30px)</div>
<div class="c-title2">
	<h3>こんにちは、<br>一級建築士事務所SHEaPの<br>戸塚治夫です。</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title3</div>
<div class="c-title3">
	<h3>Des<span>i</span>ng</h3>
	<p>6つの設計コンセプト</p>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title3 c-title3--color1</div>
<div class="c-title3 c-title3--color1">
	<h3>G<span>a</span>llery</h3>
	<p>設計事例と提案事例</p>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title4</div>
<div class="c-title4">
	<h3>1. <span>聞く</span></h3>
	<p>基本計画段階 1.5〜2ヶ月</p>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title5</div>
<div class="c-title5">
	<h3>Q1.</h3>
	<p>設計事務所って高いんじゃないの？</p>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title6</div>
<div class="c-title6">
	<img src="/assets/image/common/icon-07.png" alt="">
	<p>お気軽にご連絡ください。</p>
</div>