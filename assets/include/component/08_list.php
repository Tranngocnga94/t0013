<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1</div>
<ul class="c-list1">
	<li><p class="c-text2 ">お引き合い・打ち合せ</p></li>
	<li>・現在の悩みと「こうなりたい」のイメージ</li>
	<li>・敷地の調査</li>
	<li>・法規チェック</li>
</ul>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list2</div>
<ul class="c-list2">
	<li>
		<div class="c-list2__img">
			<img src="/assets/image/common/img-03.png" alt="">
		</div>
		<div class="c-list2__text">
			<div class="c-title4">
				<h3>1. <span>聞く</span></h3>
				<p>基本計画段階 1.5〜2ヶ月</p>
			</div>
			<ul class="c-list1">
				<li><p class="c-text2 ">お引き合い・打ち合せ</p></li>
				<li>・現在の悩みと「こうなりたい」のイメージ</li>
				<li>・敷地の調査</li>
				<li>・法規チェック</li>
			</ul>
			<ul class="c-list1">
				<li><p class="c-text2 ">プレゼンテーション</p></li>
				<li>・基本計画図（配置図・平面図・立面図</li>
				<li>・イメージスケッチ等</li>
				<li>・設計業務 概算のお見積もり</li>
			</ul>
		</div>
	</li>
</ul>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list3</div>
<ul class="c-list3">
	<li>
		<div class="c-list3__img">
			<img src="/assets/image/common/img-04.png" alt="" width="221" height="125">
		</div>
		<div class="c-list3__text">
			<h4>Q1.</h4>
			<p>設計事務所って<br>高いんじゃないの？</p>
		</div>
	</li>
	<li>
		<div class="c-list3__img">
			<img src="/assets/image/common/img-04.png" alt="" width="221" height="125">
		</div>
		<div class="c-list3__text">
			<h4>Q2.</h4>
			<p>工事は請け負わないの？</p>
		</div>
	</li>
	<li>
		<div class="c-list3__img">
			<img src="/assets/image/common/img-04.png" alt="" width="221" height="125">
		</div>
		<div class="c-list3__text">
			<h4>Q3</h4>
			<p>小さな会社に依頼しても<br>安心できますか？</p>
		</div>
	</li>
</ul>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list4</div>
<ul class="c-list4">
	<li>
		<div class="c-title6">
			<img src="/assets/image/common/icon-07.png" alt="">
			<p>お気軽にご連絡ください。</p>
		</div>
		<div class="c-list4__content">
			<div class="c-list4__img">
				<img src="/assets/image/common/img-05.png" alt="">
			</div>
			<div class="c-list4__text">
				<p><span>まずは、お気軽にご連絡ください。</span><br>実際の暮らしを体感していただくことで、間取り、光や風の入り方、生活動線などの空間イメージや設備機器、建材などの素材のイメージの参考にしていただきやすいかと思います。<br>自分らしい住まいづくりのスタートです。</p>
			</div>
		</div>
	</li>
</ul>